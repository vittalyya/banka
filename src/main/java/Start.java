import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/********************************************************************************
 * Class Start , který spouští celou aplikace
 *  Author: Andrii and Vika
 * Winter 2020
 * */
public class Start extends Application
{
    public static Stage primaryStage = null;
    public static FXMLLoader loader = null;
    public static Class forBase = null;
    @Override
    public void start(Stage primaryStage) throws Exception {
        forBase = getClass();
        Base.createFile(Base.url);
        Base.initializeBaze();
        FXMLLoader loader = new FXMLLoader();
        this.primaryStage = primaryStage;
        this.loader = loader;
        loader.setLocation(getClass().getResource("/prihlaseni.fxml"));

        AnchorPane root =  loader.load();

        Controller controller = loader.getController();

        Scene home = new Scene(root);
        primaryStage.setScene(home);
        primaryStage.show();
    }
}