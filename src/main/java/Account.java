
/****************************************************************************
 * Class Account, ve kterem se nachazí udaje o jednotlivem uživateli banku
 * Class taky obsahuje metody potřebné pro najdění ho v HashMapu a pro kvalitu prace s nim
 * Author: Andrii and Vika
 * Winter 2020
 * */
public class Account {
    private int id;
    private Klient klient;
    private long penize;

    public Account(int id, Klient klient, long penize) {
        this.id = id;
        this.klient = klient;
        this.penize = penize;
    }

    public int getID() {
        return id;
    }

    public long getPenize() {
        return penize;
    }

    public Klient getKlient() {
        return klient;
    }

    public void setID(int id) {
        this.id = id;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    public void setPenize(long penize) {
        this.penize = penize;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;

        return klient != null && klient.equals(account.klient) &&
                id == account.id && penize == account.penize;

    }

    @Override
    public int hashCode() {
        int hashCode = 9;
        hashCode = 9 * hashCode + klient.hashCode();
        hashCode = 9 * hashCode + id;
        Long penizze = penize;
        hashCode = 9 * hashCode + penizze.hashCode();
        return hashCode;
    }
    public String toString(){
        String out = "";
        out = out + id + "\n" + klient + penize;
        return out;
    }
}
