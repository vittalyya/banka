/****************************************************************************
 * Class Tranzakce, objekt ktereho se použivá při zpracování převedení peněz z učtu na učet
 * Class taky obsahuje metody potřebné  pro kvalitu prace s nim
 * Author: Andrii and Vika
 * Winter 2020
 * */
public class Tranzakce {
   private int idKK;
    private int idOK;
   private long castka;
   public Tranzakce(int idOK,int idKK, long castka){
       this.castka = castka;
       this.idKK = idKK;
       this.idOK = idOK;
   }
   public int getIdKK(){
        return idKK;
    }
    public int getIdOK(){
        return idOK;
    }
   public long getCastka(){
       return castka;
   }
}