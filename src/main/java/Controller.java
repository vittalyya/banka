import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
/****************************************************************************
 * Class Controller, který spravuje GUJ
 * Class taky obsahuje metody potřebné pro ovladáání prvku, dotazování na Base
 * Author: Andrii and Vika
 * Winter 2020
 * */
public class Controller {
    private static Account account;
    private static Key key;
    private static Account helpAccount;
    private static Account helpAccount2;
    private static Key key1;
    private static Key key2;
    /*metoda pro vytvoření Allertu informace*/
    private void showAlertWithHeaderText() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Problem s autentifikace");
        alert.setContentText("Vvedené heslo nebo login není spravné!");
        alert.showAndWait();
    }
    /*metoda pro zpracování loginu ta hesla a pro přechod na další scenu v zavislosti je-li
    * uživatel pracovnikem*/
    public void zpracujVstup(ActionEvent actionEvent) throws IOException {
        System.out.println(JmenoPrihlaseni.getText());
        System.out.println(HesloPrihlaseni.getText());
        Key key = new Key(JmenoPrihlaseni.getText(), HesloPrihlaseni.getText());
        this.key = key;
        if (Base.getKeyBase().contains(key)) {
            account = Base.getAccount(key);
            if (account.getKlient().getJePracovnikem() == false) {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/klient.fxml"));
                AnchorPane root = loader.load();

                Controller controller = loader.getController();

                Scene home = new Scene(root);
                Start.primaryStage.setScene(home);
                Start.primaryStage.show();
            }
            if (account.getKlient().getJePracovnikem() == true) {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/pracovnik.fxml"));
                AnchorPane root = loader.load();

                Controller controller = loader.getController();

                Scene home = new Scene(root);
                Start.primaryStage.setScene(home);
                Start.primaryStage.show();
            }
        } else {
            showAlertWithHeaderText();
        }
    }
     /*metoda pro odhlašení z aplikace*/
    public void seOdhlasitP(ActionEvent actionEvent) throws IOException {
        helpAccount = null;
        Base.initializeBaze();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/prihlaseni.fxml"));

        AnchorPane root = loader.load();

        Controller controller = loader.getController();

        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro odhlašení z aplikace*/
    public void seOdhlasitK(ActionEvent actionEvent) throws IOException {
        Base.initializeBaze();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/prihlaseni.fxml"));

        AnchorPane root = loader.load();

        Controller controller = loader.getController();

        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k spravci plateb*/
    public void provestPlatbu(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/platba.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k menu klient*/
    public void zpetKeKlientu(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/klient.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda, která spravuje platby*/
    public void odeslatKzpracovani(ActionEvent actionEvent) throws IOException {
        int cukk1;
        int cuok;
        long castka2;
        try {
            String cukk = CisloUctuPlatba.getText();
            cukk1 = Integer.parseInt(cukk);
            cuok = Controller.account.getID();
            String castka = CastkaPlatba.getText();
            castka2 = Long.parseLong(castka);

        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Není spravný format");
            alert.setContentText("Zadané udaje nejsou ve spravnem formatu");
            alert.showAndWait();
            return;
        }

        Tranzakce tranzakce = new Tranzakce(cuok, cukk1, castka2);
        if (Base.controlTranzakce(tranzakce) == 1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Není v baze");
            alert.setContentText("Zadaný účet, na který odesilate penize není v systemu");
            alert.showAndWait();
        }
        if (Base.controlTranzakce(tranzakce) == 2) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Není dost peněz");
            alert.setContentText("Pro zadanou tranzakce na vašem účtu není dost peněz");
            alert.showAndWait();
        }
        if (Base.controlTranzakce(tranzakce) == 3) {
            Base.spracovatTranzakce(tranzakce);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Operace proběhla uspěšně");
            alert.setContentText("Zadaná tranzakce byla přijatá ke zpracování");
            alert.showAndWait();
            CisloUctuPlatba.clear();
            CastkaPlatba.clear();
        }
    }
/*metoda která převádí uživatele na spravce vytvoření nového klienta*/
    public void vytvarimeNK(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader;
        loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/vytvoreni.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k menu Pracovnik*/
    public void zpetKPracovnikovi1(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/pracovnik.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*spravce pro vytvoření noveho klienta*/
    public void vytvarimeNK2(ActionEvent actionEvent) throws IOException {

        String jmeno = JmenoVytvoreni.getText();
        String prijmeni = PrijmeniVytvoreni.getText();
        String adresa = AdresaVytvoreni.getText();
        String pracovnik = PracovnikVytvoreni.getText();
        String heslo = HesloVytvoreni.getText();
        String login = LoginVytvoreni.getText();
        boolean jePracovnikem;
        long penize;
        int id;
        try {
            penize = Long.parseLong(BalanceVytvoreni.getText());
            id = Integer.parseInt(IdVytvoreni.getText());
            if (pracovnik.equals("Ano")) {
                jePracovnikem = true;
            } else {
                jePracovnikem = false;
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Nejsou spravné udajé");
            alert.setContentText("Zadané udaje nejsou ve spravnem formatu");
            alert.showAndWait();
            return;
        }
        Klient klient = new Klient(jmeno, prijmeni, adresa, jePracovnikem);
        Account account = new Account(id, klient, penize);
        Key key = new Key(login, heslo);
        if (Base.controlID(id) == true) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Zadaný id už existujé");
            alert.setContentText("Zadaný id už existujé v systemu");
            alert.showAndWait();
            return;
        }
        if (Base.controlLogin(login) == true) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Zadaný login už existujé");
            alert.setContentText("Zadaný login už existujé v systemu");
            alert.showAndWait();
            return;
        }
        Base.addNewAccount(key, account);
        Base.updateBase();
        Base.initializeBaze();
        JmenoVytvoreni.clear();
        PrijmeniVytvoreni.clear();
        AdresaVytvoreni.clear();
        PracovnikVytvoreni.clear();
        HesloVytvoreni.clear();
        LoginVytvoreni.clear();
        BalanceVytvoreni.clear();
        IdVytvoreni.clear();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Operace proběhlá uspěšně");
        alert.setContentText("Přidání uživatele do systemu proběhlo uspěšně");
        alert.showAndWait();
    }
    /*metoda pro přechod k menu klient*/
    public void zpetOdUpraveniFK(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/klient.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();

    }
    /*metoda pro přechod k menu klient nebo Pracovnik*/
    public void zpetF(ActionEvent actionEvent) throws IOException {
        if(account.getKlient().getJePracovnikem() == true){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/pracovnik.fxml"));
            AnchorPane root = loader.load();
            Controller controller = loader.getController();
            Scene home = new Scene(root);
            Start.primaryStage.setScene(home);
            Start.primaryStage.show();
        } else {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/klient.fxml"));
            AnchorPane root = loader.load();
            Controller controller = loader.getController();
            Scene home = new Scene(root);
            Start.primaryStage.setScene(home);
            Start.primaryStage.show();
        }
    }
    /*metoda pro přechod k menu  Pracovnik*/
    public void zpetOdUpraveniFP(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/pracovnik.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();

    }
    /*metoda pro přechod k info*/
    public void kInfo(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/info.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k balansu*/
    public void kZustatku(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/balance.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro ovladaní tlačitka, které obnovuje udaje o balansi*/
    public void obnovitBalanse(ActionEvent actionEvent) throws IOException {
        Base.updateBase();
        Base.initializeBaze();
        ZustatekBalance.setText(account.getPenize() + "");
    }
    /*metoda pro přechod k upravení info o sobě pro klienta*/
    public void upravitInfoOsobe(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/upraveniFK.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
        System.out.println(Controller.key.getHeslo());

    }
    /*metoda pro přechod k upravení info o sobě pro pracovnika*/
    public void upravitInfoOsobeFP(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/upraveniFP.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k panelu zadání id*/
    public void kid(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/id.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k panelu zadání id*/
    public void kidu(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/idU.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro přechod k upravení pracovnikem uivatele*/
    public void uiou(ActionEvent actionEvent) throws IOException {
     String id = ZadejteId.getText();
     try {
         helpAccount = Base.najitUzivatele(Integer.parseInt(id));
         key1 = Base.najitKey(Integer.parseInt(id));
     }catch(Exception e){
         Alert alert = new Alert(Alert.AlertType.WARNING);
         alert.setTitle("Nespravný format");
         alert.setContentText("Uvedli jste nespravný format pro ID");
         alert.showAndWait();
         return;
     }
     if(helpAccount == null){
         Alert alert = new Alert(Alert.AlertType.WARNING);
         alert.setTitle("Není uživatel");
         alert.setContentText("Uvedli jste ID, které není v systemu");
         alert.showAndWait();
         return;
     }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/upraveniU.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();

    }
    /*metoda pro přechod k prohližení info pracovnikem uivatele*/
    public void uiou2(ActionEvent actionEvent) throws IOException {

        String id = ZadejteId.getText();
        try {
            helpAccount = Base.najitUzivatele(Integer.parseInt(id));
            key1 = Base.najitKey(Integer.parseInt(id));
        }catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Nespravný format");
            alert.setContentText("Uvedli jste nespravný format pro ID");
            alert.showAndWait();
            return;
        }
        if(helpAccount == null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Není uživatel");
            alert.setContentText("Uvedli jste ID, které není v systemu");
            alert.showAndWait();
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/infoU.fxml"));
        AnchorPane root = loader.load();
        Controller controller = loader.getController();
        Scene home = new Scene(root);
        Start.primaryStage.setScene(home);
        Start.primaryStage.show();
    }
    /*metoda pro zpracování změny pracovnikem uživatele*/
    public void zpracovaniZmenyU(ActionEvent actionEvent) throws IOException {
        key2 = key;
        key = key1;
        helpAccount2 = account;
        account = helpAccount;
        String heslo = HesloUpraveni.getText();
        String adresa = AdresaUpraveni.getText();
        String jmeno = JmenoUpraveni.getText();
        String prijmeni = PrijmeniUpraveni.getText();
        if (heslo.equals("") || adresa.equals("") || jmeno.equals("") || prijmeni.equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Propouštěné řadky");
            alert.setContentText("Některe z řadku jsou propouštěné a tak nelze změnu udělat");
            alert.showAndWait();
            return;
        }
        if (heslo.equals(key.getHeslo()) && adresa.equals(account.getKlient().getAdresa()) &&
                jmeno.equals(account.getKlient().getJmeno()) && prijmeni.equals(account.getKlient().getPrijmeni())) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Níc jste nezměnily");
            alert.setContentText("Níc jste nezměnily, tak že není co měnit");
            alert.showAndWait();
            return;
        }
        Key key = new Key(this.key.getLogin(), heslo);
        Klient klient = new Klient(jmeno, prijmeni, adresa, account.getKlient().getJePracovnikem());
        Account account = new Account(this.account.getID(), klient, this.account.getPenize());

        Base.removeAccount(this.key);
        this.account = account;
        this.key = key;
        Base.addNewAccount(key, account);
        Base.updateBase();
        Base.initializeBaze();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Operace proběhlá uspěšně");
        alert.setContentText("Změna v osobních udajích proběhlá uspěšně");
        alert.showAndWait();
        this.account = helpAccount2;
        helpAccount2 = null;
        this.key = key2;
        Base.updateBase();
        Base.initializeBaze();
    }
    /*metoda pro ovladaní tlačitky, ktera obnovuje info o uživateli */
    public void infoOme(ActionEvent actionEvent) throws IOException {
        JmenoInfo.setText(account.getKlient().getJmeno());
        PrijmeniInfo.setText(account.getKlient().getPrijmeni());
        AdresaInfo.setText(account.getKlient().getAdresa());
        String jp = null;
        if(account.getKlient().getJePracovnikem() == true){
            jp = "Ano";
        } else {
            jp = "Ne";
        }
        PracovnikInfo.setText(jp);
        IdInfo.setText(account.getID() + "");
        BalanceInfo.setText(account.getPenize() + "");
        LoginInfo.setText(key.getLogin());
        HesloInfo.setText(key.getHeslo());
    }
    /*metoda pro ovladaní tlačitky, ktera obnovuje info o uživateli pro pracovnika*/
    public void infoOnme(ActionEvent actionEvent) throws IOException {
        JmenoInfo.setText(helpAccount.getKlient().getJmeno());
        PrijmeniInfo.setText(helpAccount.getKlient().getPrijmeni());
        AdresaInfo.setText(helpAccount.getKlient().getAdresa());
        String jp = null;
        if(helpAccount.getKlient().getJePracovnikem() == true){
            jp = "Ano";
        } else {
            jp = "Ne";
        }
        PracovnikInfo.setText(jp);
        IdInfo.setText(helpAccount.getID() + "");
        BalanceInfo.setText(helpAccount.getPenize() + "");
        LoginInfo.setText(key1.getLogin());
        HesloInfo.setText(key1.getHeslo());
    }
    /*pomocná metoda pro vyvod na ekran info o sobě*/
    public void obnovitA(ActionEvent actionEvent) throws IOException {
        HesloUpraveni.setText(Controller.key.getHeslo());
        AdresaUpraveni.setText(account.getKlient().getAdresa());
        JmenoUpraveni.setText(account.getKlient().getJmeno());
        PrijmeniUpraveni.setText(account.getKlient().getPrijmeni());
    }
    /*pomocná metoda pro vyvod na ekran info o uživateli pracovnikem*/
    public void obnovitB(ActionEvent actionEvent) throws IOException {
        helpAccount2 = account;
        account = helpAccount;
        HesloUpraveni.setText(Base.najitHeslo(account.getID()));
        AdresaUpraveni.setText(account.getKlient().getAdresa());
        JmenoUpraveni.setText(account.getKlient().getJmeno());
        PrijmeniUpraveni.setText(account.getKlient().getPrijmeni());
        account = helpAccount2;
    }
    /*metoda která spravuje změnu o uživateli*/
    public void zpracovaniZmeny(ActionEvent actionEvent) throws IOException {
        String heslo = HesloUpraveni.getText();
        String adresa = AdresaUpraveni.getText();
        String jmeno = JmenoUpraveni.getText();
        String prijmeni = PrijmeniUpraveni.getText();
        if(heslo.equals("") || adresa.equals("") || jmeno.equals("") || prijmeni.equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Propouštěné řadky");
            alert.setContentText("Některe z řadku jsou propouštěné a tak nelze změnu udělat");
            alert.showAndWait();
            return;
        }
        if(heslo.equals(key.getHeslo()) && adresa.equals(account.getKlient().getAdresa()) &&
        jmeno.equals(account.getKlient().getJmeno()) && prijmeni.equals(account.getKlient().getPrijmeni())){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Níc jste nezměnily");
            alert.setContentText("Níc jste nezměnily, tak že není co měnit");
            alert.showAndWait();
            return;
        }
        Key key = new Key(this.key.getLogin(), heslo);
        Klient klient = new Klient(jmeno, prijmeni, adresa, account.getKlient().getJePracovnikem());
        Account account = new Account(this.account.getID(), klient, this.account.getPenize());

        Base.removeAccount(this.key);
        this.account = account;
        this.key = key;
        Base.addNewAccount(key, account);
        Base.updateBase();
        Base.initializeBaze();
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Operace proběhlá uspěšně");
        alert.setContentText("Změna v osobních udajích proběhlá uspěšně");
        alert.showAndWait();
    }
    @FXML
     private Text ZustatekBalance;
    @FXML
    private TextField JmenoPrihlaseni;
    @FXML
    private PasswordField HesloPrihlaseni;


    @FXML
    private Button OdhlaseniKlient;
    @FXML
    private Button UpravitInfoKlient;
    @FXML
    private Button ZustatekKlient;
    @FXML
    private Button ProvestPlatbuKlient;
    @FXML
    private Button ProhlednoutInfoKlient;


    @FXML
    private Button InfoKlientPracovnik;
    @FXML
    private Button UpravitSvojeInfoPracovnik;
    @FXML
    private Button NovyKlientPracovnik;
    @FXML
    private Button UpravitInfoKlientaPracovnik;
    @FXML
    private Button InfoPracovnik;
    @FXML
    private Button OdhlaseniPracovnik;


    @FXML
    private TextField CastkaPlatba;
    @FXML
    private Button EnterPlatba;
    @FXML
    private TextField CisloUctuPlatba;
    @FXML
    private Button ZpetPlatba;


    @FXML
    private TextArea JmenoInfo;

    @FXML
    private TextArea PracovnikInfo;

    @FXML
    private TextArea BalanceInfo;

    @FXML
    private Button ZpetInfo;

    @FXML
    private TextArea PrijmeniInfo;

    @FXML
    private TextArea AdresaInfo;

    @FXML
    private TextArea LoginInfo;

    @FXML
    private TextArea HesloInfo;

    @FXML
    private TextArea IdInfo;

    @FXML
    private Button EnterUpraveni;
    @FXML
    private TextField HesloUpraveni;
    @FXML
    private Button ZpetUpraveni;
    @FXML
    private TextField JmenoUpraveni;
    @FXML
    private TextField AdresaUpraveni ;
    @FXML
    private TextField PrijmeniUpraveni;


    @FXML
    private TextField JmenoVytvoreni;
    @FXML
    private TextField HesloVytvoreni;
    @FXML
    private Button ZpetVytvoreni;
    @FXML
    private TextField LoginVytvoreni;
    @FXML
    private TextField AdresaVytvoreni;
    @FXML
    private TextField PracovnikVytvoreni;
    @FXML
    private TextField PrijmeniVytvoreni;
    @FXML
    private TextField BalanceVytvoreni;
    @FXML
    private Button EnterVytvoreni;
    @FXML
    private TextField IdVytvoreni;


    @FXML
    private Button EnterId;
    @FXML
    private TextField ZadejteId;
    @FXML
    private Button ZpetId;
    @FXML
    private Button obnovit;
    @FXML
    private Button ObnovitBalance;
    @FXML
    private Button ObnovitInfo;
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private Button ButtonPrihlaseni;
}
