import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
/****************************************************************************
* Class Base, ve kterem se nachazí udaje z failu base.txt
* Class taky obsahuje metody potřebné pro vvod a vyvod z failu base.txt
* a metody potřebné pro vyhledání informace pro Controller
* Author: Andrii and Vika
* Winter 2020
* */
public class Base {

    public static URL url  = Start.forBase.getResource("/base.txt");
    private static  File file = new File(createFile(url));
    private static HashMap<Key, Account> accounts = new HashMap<>();
    private static HashSet<Key> keyBase = new HashSet();
    public static Account getAccount(Key key) {
        return accounts.get(key);
    }

    public  static HashMap<Key, Account> getAccounts(){
        return accounts;
    }
    public static String createFile(URL url){
        String string = url.toString();
        String [] help  = string.split("/", -1);
        String out = "";
        for(int x = 1; x < help.length; x++){
            if(x == help.length - 1){
                out = out + help[x];
            }
            else {
                out = out + help[x] + "\\\\";
            }
        }
        return  out;
    }
    public static String najitHeslo(int id){
        String heslo = null;
        for (Key key:keyBase) {
            Account account2 = accounts.get(key);
            if(account2.getID() == id){
                return key.getHeslo();
            }
        }
        return heslo;

    }
    public static Key najitKey(int id){
        Key key1 = null;
        for (Key key:keyBase) {
            Account account2 = accounts.get(key);
            if(account2.getID() == id){
                return key;
            }
        }
        return key1;

    }
    public static Account najitUzivatele(int id){
        Account account = null;
            for (Key key:keyBase) {
                Account account2 = accounts.get(key);
                if(account2.getID() == id){
                    return account2;
                }
            }
            return account;

    }
    public static void removeAccount(Key key){
        accounts.remove(key);
        keyBase.remove(key);
    }
    public static void addNewAccount(Key key, Account account){
        accounts.put(key, account);
        keyBase.add(key);
    }
    public static boolean controlID(int id){
        for (Key key:keyBase) {
            Account account = accounts.get(key);
            if(account.getID() == id){
                return true;
            }
        }
        return false;
    }
    public static boolean controlLogin(String login){
        for (Key key:keyBase) {

            if(key.getLogin().equals(login)){
                return true;
            }
        }
        return false;
    }
    public static int controlTranzakce(Tranzakce tranzakce){
        int idkk = 0;
        boolean neniPenize = false;
        for (Key key:keyBase) {
            Account account = accounts.get(key);
            if(account.getID() == tranzakce.getIdKK()){
                idkk = tranzakce.getIdKK();
            }
            if(account.getID() == tranzakce.getIdOK() && account.getPenize() < tranzakce.getCastka()){
                neniPenize = true;
            }
        }
        if(idkk == 0){
            return 1;
        }
        if(neniPenize == true){
            return 2;
        }
        return 3;

}
    private static ArrayList<String> read(){
        ArrayList<String> out = new ArrayList<>();
        Scanner scanner = null;
        try  {
            scanner = new Scanner(file);
            while(scanner.hasNextLine()){
                String person = "";
                String line = scanner.nextLine();
                while(line.equals("-") == false) {
                    person = person + line + " ";
                    line = scanner.nextLine();
                }
                out.add(person);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            scanner.close();
        }

        return out;
    }
    public static void initializeBaze(){
        ArrayList<String>out = read();
        for (String person : out ) {
            String [] help  = person.split(" ", -1);
            boolean jepr = help[8].equals("Ano") ? true : false;
            Klient klient = new Klient(help[1], help[3], help[5] + " " + help[6], jepr);
            Account account = new Account(Integer.parseInt(help[10]), klient, Long.parseLong(help[12]));
            Key key = new Key(help[14], help[16]);
            accounts.put(key, account);
            keyBase.add(key);
        }
    }
    public static void spracovatTranzakce(Tranzakce tranzakce){
        for (Key key:keyBase) {
            Account account = accounts.get(key);
            if(account.getID() == tranzakce.getIdOK()){
                account.setPenize(account.getPenize() - tranzakce.getCastka());
            }
            if(account.getID() == tranzakce.getIdKK()){
                account.setPenize(account.getPenize() + tranzakce.getCastka());
            }
        }
        updateBase();
        initializeBaze();
    }
    public static void updateBase() {
        PrintWriter printWriter = null;
        try {
            printWriter =  new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (Key key:keyBase) {
            Account account = accounts.get(key);
            printWriter.println("Jmeno: " + account.getKlient().getJmeno());
            printWriter.println("Prijmeni: " + account.getKlient().getPrijmeni());
            printWriter.println("Adresa: " + account.getKlient().getAdresa());
            if(account.getKlient().getJePracovnikem() == false){
                printWriter.println("jePracovnikem: " + "Ne");
            }
            if(account.getKlient().getJePracovnikem() == true){
                printWriter.println("jePracovnikem: " + "Ano");
            }
            printWriter.println("ID: " + account.getID());
            printWriter.println("Penize: " + account.getPenize());
            printWriter.println("login: " + key.getLogin());
            printWriter.println("heslo: " + key.getHeslo());
            printWriter.println("-");
        }
        printWriter.close();
    }

    public static HashSet<Key> getKeyBase(){
        return keyBase;
    }
}
