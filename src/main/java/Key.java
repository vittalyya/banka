/****************************************************************************
 * Class Key, objekt kterého nese v sobě informace o loginu a heslu uživatele
 * Class taky obsahuje metody potřebné pro najdění ho a pro kvalitu prace s nim
 * Author: Andrii and Vika
 * Winter 2020
 * */
public class Key {
    private String login;
    private String heslo;

    public Key(String login, String heslo) {
        this.login = login;
        this.heslo = heslo;
    }

    public String getLogin() {
        return login;
    }

    public String getHeslo() {
        return heslo;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }
    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Key)) {
            return false;
        }
        Key key = (Key) o;

        return login != null && login.equals(key.login) &&
                heslo != null && heslo.equals(key.heslo);

    }

    @Override
    public int hashCode() {
        int hashCode = 2;
        hashCode = 2 * hashCode + login.hashCode();
        hashCode = 2 * hashCode + heslo.hashCode();
        return hashCode;
    }
}
