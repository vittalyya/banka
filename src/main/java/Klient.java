/****************************************************************************
 * Class Klient, ve kterem se nachazí veškera informace o uživateli
 * Class taky obsahuje metody potřebné pro najdění ho  a pro kvalitu prace s nim
 * Author: Andrii and Vika
 * Winter 2020
 * */
public class Klient {
    private String jmeno;
    private String prijmeni;
    private String adresa;
    private boolean jePracovnikem;

    public Klient(String jmeno, String prijmeni, String adresa, boolean jePracovnikem) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
        this.adresa = adresa;
        this.jePracovnikem = jePracovnikem;
    }

    public String getJmeno() {
        return jmeno;
    }

    public String getPrijmeni() {
        return prijmeni;
    }

    public String getAdresa() {
        return adresa;
    }

    public boolean getJePracovnikem() {
        return jePracovnikem;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public void setJePracovnikem(boolean jePracovnikem) {
        this.jePracovnikem = jePracovnikem;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Klient)) {
            return false;
        }
        Klient klient = (Klient) o;

        return jmeno != null && jmeno.equals(klient.jmeno) &&
                jePracovnikem == klient.jePracovnikem &&
                prijmeni != null && prijmeni.equals(klient.prijmeni) &&
                adresa != null && adresa.equals(klient.adresa);

    }

    @Override
    public int hashCode() {
        int hashCode = 5;
        hashCode = 5 * hashCode + jmeno.hashCode();
        hashCode = 5 * hashCode + prijmeni.hashCode();
        hashCode = 5 * hashCode + adresa.hashCode();
        Boolean klientsky = jePracovnikem;
        hashCode = 5 * hashCode + klientsky.hashCode();
        return hashCode;
    }
   public  String toString(){
        String out = "";
        out = out + jmeno + "\n" + prijmeni + "\n" + jePracovnikem + "\n" + adresa +"\n";
        return out;
    }
}
