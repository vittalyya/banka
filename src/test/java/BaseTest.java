import org.junit.*;
import org.junit.rules.ExpectedException;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BaseTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Testováni se začalo");
    }

    @Before
    public void setUp() {
        System.out.println("Testovaní konkretní metody se začalo");
        Klient klient1 = new Klient("Andrii", "Tuma", "Vasylkivska 38", false);
        Klient klient2 = new Klient("Viktoria", "Lozian", "Zagorodnogo 79", true);
        Klient klient3 = new Klient("Filip", "Vencovsky", "Shevchenka 68", false);
        Account account1 = new Account(3232, klient1, 20000);
        Account account2 = new Account(3233, klient2, 20000);
        Account account3 = new Account(3234, klient3, 100000);
        Key key1 = new Key("1", "1");
        Key key2 = new Key("2", "2");
        Key key3 = new Key("3", "3");
        Start.forBase = getClass();
        Base.createFile(Base.url);
        Base.getAccounts().put(key1, account1);
        Base.getAccounts().put(key2, account2);
        Base.getAccounts().put(key3, account3);
        Base.getKeyBase().add(key1);
        Base.getKeyBase().add(key2);
        Base.getKeyBase().add(key3);
    }
    @Test
    public void controlMetodNajitKey(){
        assertThat(Base.najitKey(3232).getLogin(), is("1"));
        assertThat(Base.najitKey(3233).getHeslo(), is("2"));
    }
    @Test
    public void controlMetodControlID(){
        assertThat(Base.controlID(3232), is(true));
        assertThat(Base.controlID(1000), is(false));
    }
    @Test
    public void controlMetodControlLogin(){
        assertThat(Base.controlLogin("1"), is(true));
        assertThat(Base.controlLogin("4"), is(false));
    }
    @Test
    public void controlMetodControlTranzakce(){
        Tranzakce tranzakce1 = new Tranzakce(3232, 3233, 10000);
        Tranzakce tranzakce2 = new Tranzakce(3232, 32350, 10000);
        Tranzakce tranzakce3 = new Tranzakce(3232, 3233, 1000000);
        assertThat(Base.controlTranzakce(tranzakce1), is(3));
        assertThat(Base.controlTranzakce(tranzakce2), is(1));
        assertThat(Base.controlTranzakce(tranzakce3), is(2));
    }
    @Test
    public void controlMetodRemoveAccount(){
        assertThat(Base.getAccounts().size(), is(3));
        assertThat(Base.getKeyBase().size(), is(3));
       Base.removeAccount(new Key("1", "1"));
        assertThat(Base.getAccounts().size(), is(2));
        assertThat(Base.getKeyBase().size(), is(2));
    }
    @AfterClass
    public static void tearDown() {
        Base.getAccounts().clear();
        Base.getKeyBase().clear();
        System.out.println("Tests finished");
    }

    @After
    public void afterMethod() {
        System.out.println("Testovaní konkretní metody se skončilo");
    }
}
